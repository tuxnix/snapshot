#!/bin/bash
##
##       
##      Creates daily, weekly and montly backups with rsync.
##
##      Version: 0.4
##
##      Original from:  Mike Rubel (http://www.mikerubel.org/computers/rsync_snapshots/)
## 
##      License:   LGPL
##
##      Disclaimer: Use this software on your own risk, it works for me but there are no guaranties at all.
## 
##      Changes from tuxnix:        *Only one systemd timer needed
##                                  *rsync.log
##                                  *Source directory choosable
##                                  
##                                              
## 
## ================ CUSTOMIZING SECTION =================================================================================

SOURCE='/home/user/ /NAS/'                           # Source needs a / at the end. More than one source write in '  '
EXCLUDES=/usr/local/bin/snapshot_exclude.txt         # full path of exludefile is needed

MOUNT_RO=true                                        # For write protection set to true
MOUNT_DEVICE=/dev/disk/by-label/SICHERUNG            # Taget device
MOUNT_POINT=/SICHERUNG                               # Mount point of target device
TARGET=/SICHERUNG                                    # Target directory - full directory path of mounted device is needed

CHECK_HDMINFREE=true;         # Check free space
HDMINFREE=98;                 # Make a backup up to this percentage

DAYS=6;                       # Number of daily backups -1
WEEKS=3;                      # Number of weekly backups -1
MONTHS=11;                    # Number of monthly backups -1


# ---------------------- Check -----------------------------------------------------------------------------------------

# Make sure we're running as root
if (( `id -u` != 0 )); then { echo "Sorry, must be root.  Exiting..."; exit; } fi

# Check exludefile exists
if ! [ -f $EXCLUDES ] ; then { echo "Error: Exclude File $EXCLUDES missing"; exit; } fi


# Remount Device rw
if $MOUNT_RO ; then
        mount -o remount,rw $MOUNT_DEVICE $MOUNT_POINT
        if (( $? )) ; then
            echo "Error: could not remount $MOUNT_DEVICE readwriteable"
            exit
        fi
fi

# Check free space on disk
GETPERCENTAGE='s/.* \([0-9]\{1,3\}\)%.*/\1/'
if $CHECK_HDMINFREE ; then
        KBISFREE=`df /$TARGET | tail -n1 | sed -e "$GETPERCENTAGE"`
        if [ $KBISFREE -ge $HDMINFREE ] ; then
                echo "Error: Not enough space left for rotating backups!"
                logger "Error: Not enough space left for rotating backups!"
                exit
        fi
fi

# Give Source some time to wake up if needed.
ls $SOURCE > /dev/null
if (( $? )) ; then
    echo "Wait 10 seconds till $SOURCE device is arrivable."
    sleep 10
    ls $SOURCE > /dev/null
    if (( $? )); then
    echo "Can't read $SOURCE"
    exit
    fi
fi

# ---------------Rotate Monthly Snapshots------------------------------------------------------------------------------

if [ -d $TARGET/weekly.$WEEKS ] && ! [ -d $TARGET/monthly.0 ] ; then

    echo "mv $TARGET/weekly.$WEEKS $TARGET/monthly.0"
    mv $TARGET/weekly.$WEEKS $TARGET/monthly.0

elif [ -d $TARGET/weekly.$WEEKS ] && [ -d $TARGET/monthly.0 ] ; then

    STAMP_0=`stat -c %Y $TARGET/weekly.$WEEKS`
    STAMP_1=`stat -c %Y $TARGET/monthly.0`

    if [ $(( ($STAMP_0 - $STAMP_1)/(60*60*24) )) -ge 30 ] ; then

           echo ""
           echo "Rotate Monthly Snapshots:"

# Step 1: Delete the oldest monthly snapshot, if it exists
           if [ -d $TARGET/monthly.$MONTHS ] ; then
                echo "rm -rf $TARGET/monthly.$MONTHS"
                rm -rf $TARGET/monthly.$MONTHS 
           fi

# Step 2: Shift the middle snapshots by one
           OLD=$MONTHS
           while [ $OLD -ge 1 ] ; do
                OLD=$[$OLD-1]
                if [ -d $TARGET/monthly.$OLD ] ; then
                        NEW=$[ $OLD + 1 ]
                        echo "mv $TARGET/monthly.$OLD $TARGET/monthly.$NEW"
                        mv $TARGET/monthly.$OLD $TARGET/monthly.$NEW
                fi
           done

# Step 3: move oldest weekly to monthly.0
            echo "mv $TARGET/weekly.$WEEKS $TARGET/monthly.0"
            mv $TARGET/weekly.$WEEKS $TARGET/monthly.0 
           
    fi
fi

# ---------------Rotate Weekly Snapshots------------------------------------------------------------------------------

if [ -d $TARGET/daily.$DAYS ] && ! [ -d $TARGET/weekly.0 ] ; then

    echo "mv $TARGET/daily.$DAYS $TARGET/weekly.0"
    mv $TARGET/daily.$DAYS $TARGET/weekly.0

elif  [ -d $TARGET/daily.$DAYS ] && [ -d $TARGET/weekly.0 ] ; then

    STAMP_0=`stat -c %Y $TARGET/daily.$DAYS`
    STAMP_1=`stat -c %Y $TARGET/weekly.0`

    #echo "$(( ($STAMP_0 - $STAMP_1)/(60*60*24) )) days difference between $TARGET/daily.$DAYS and $TARGET/weekly.0"

    if [ $(( ($STAMP_0 - $STAMP_1)/(60*60*24) )) -ge 7 ] ; then

           echo ""
           echo "Rotate Weekly Snapshots:"

# Step 1: Delete the oldest weekly snapshot, if it exists
        if [ -d $TARGET/weekly.$WEEKS ] ; then
            echo "rm -rf $TARGET/weekly.$WEEKS"
            rm -rf $TARGET/weekly.$WEEKS
        fi

# Step 2: shift the middle snapshots by one.
        OLD=$WEEKS
        while [ $OLD -ge 1 ] ; do
                OLD=$[ $OLD - 1 ]
                if [ -d $TARGET/weekly.$OLD ] ; then
                        NEW=$[ $OLD + 1 ]
                        echo "mv $TARGET/weekly.$OLD $TARGET/weekly.$NEW"
                        mv $TARGET/weekly.$OLD $TARGET/weekly.$NEW
                fi
        done

# Step 3: move oldest daily to weekly.0
        echo "mv $TARGET/daily.$DAYS $TARGET/weekly.0"
        mv $TARGET/daily.$DAYS $TARGET/weekly.0
    fi
fi

# -----------------Rotate Daily Snapshots---------------------------------------------

           echo ""
           echo "Rotate Daily Snapshots:"

           if [ -d $TARGET/daily.$DAYS ] ; then
           echo "rm -rf $TARGET/daily.$DAYS"
                rm -rf $TARGET/daily.$DAYS
           fi

           OLD=$DAYS
           while [ $OLD -ge 2 ] ; do
                OLD=$[$OLD-1]
                if [ -d $TARGET/daily.$OLD ] ; then
                        NEW=$[ $OLD + 1 ]
                        echo "mv $TARGET/daily.$OLD $TARGET/daily.$NEW"
                        mv $TARGET/daily.$OLD $TARGET/daily.$NEW
                fi
           done

           echo ""
           echo "Copy Hardlinks:"
           if ! [ -d $TARGET/daily.0 ] ; then
                $MKDIR -p $TARGET/daily.0
           else
                echo "cp -al $TARGET/daily.0 $TARGET/daily.1"
                cp -al $TARGET/daily.0 $TARGET/daily.1 ;
                touch $TARGET/daily.1 --reference=$TARGET/daily.1/.timestamp
          fi

# -----------------Start Backup--------------------------------------------------------------------------------------------------
           echo ""
           echo "Start Backup:"
           rsync -va --delete --delete-excluded --exclude-from="$EXCLUDES" --log-file=$TARGET/.rsync.log $SOURCE $TARGET/daily.0

           if ! [ $? = 24 -o $? = 0 ] ; then
                echo "rsync ended with ERRORS!"
                logger "rsync ended with ERRORS!"
                touch $TARGET/daily.0/.rsync_error
                touch $TARGET/daily.0/.timestamp
                exit
             else
                touch $TARGET/daily.0/.timestamp
                touch $TARGET/daily.0
                echo "Backup succeeded"
                logger "Backup succeeded"
           fi

if $MOUNT_RO ; then
        #echo "mount -o remount,ro $MOUNT_DEVICE $TARGET"
        mount -o remount,ro $MOUNT_DEVICE $TARGET
        if (( $? )); then
                echo "Error: Could not remount $TARGET readonly"
                exit
        fi
fi
