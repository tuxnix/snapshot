# snapshot
Creates daily, weekly and montly backups with rsync.

Version: 0.4

Original from:  
Mike Rubel http://www.mikerubel.org/computers/rsync_snapshots<br>
License:   LGPL<br>
With no guaranties at all.<br>
 
Changes from tuxnix:
* Only one systemd timer needed
* rsync.log
* Source directory choosable
